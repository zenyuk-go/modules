package hello

import (
	"fmt"

	"rsc.io/quote"
)

// Hello learn modules-packages-exported-func
func Hello() string {
	fmt.Println("hello was called")
	return "hello" + quote.Hello()
}
