package hello

import (
	"testing"
)

func TestHello(t *testing.T) {
	want := "helloHello, world."
	got := Hello()
	if want != got {
		t.Errorf("Hello() = %q, want %q", got, want)
	}
}
