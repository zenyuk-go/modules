"go get" is depricated since 1.17.1, use "go install" 
while in module path execute below (w/o specifying version)
==
go install example.com/cmd


install "go program" gloabally (not for a module) - specify version, e.g. "latest"
==
go install example.com/cmd@latest


dependency from a git commit revision
figure out go module depency in format "github.com/ipfs/go-ipfs v0.9.2-0.20210803161547-e9b1e2d6fb91"
when you know only a commit hash
==
navigate to your module, which requires the dependency with a particular git revision
from the path, execute:
go get github.com/bla/a_module@abc123abc123

your go.mod will be updated


show dependency hierarchy/chain/tree
==
go mod graph
or 
go list -deps
or 
go mod why githib.com/bla/your_package


in the example below example.com/a@v1.1.0 has 2 direct deps: example.com/b@v1.1.1 and example.com/c@v1.3.0

example.com/main example.com/a@v1.1.0
example.com/main example.com/b@v1.2.0
example.com/a@v1.1.0 example.com/b@v1.1.1
example.com/a@v1.1.0 example.com/c@v1.3.0
example.com/b@v1.1.0 example.com/c@v1.1.0
example.com/b@v1.2.0 example.com/c@v1.2.0


use multiple versions of the same dependency simultaniously
==
Packages with major version > 1 must be imported with an import path ending in "/v2". You cannot have the same package (identified by its import path) in two different versions. That's why a different version needs a different import path
(specify v2 in both locations: 
    1) in go.mod 
    2) in import)


force to use a particular version of a dependency
==
you have a direct dependency A, which has another B, and B has other - "go-data-transfer" of version v1.1.1
you want that everybody be using "go-data-transfer" of version v1.7.2

in your go.mod file:
github.com/bla/go-data-transfer v1.7.2 // indirect


update dependency on a module
==
go get github.com/ConsenSys/fc-retrieval-common
go mod download github.com/ConsenSys/fc-retrieval-common


find all versions of locally installed modules 
(instead of using `replace`)
==
cd $GOPATH
cd pkg/mod/github.com/
ll | grep YOUR_PART 
# all modules will be listed, e.g.:
# .... fc-retrieval-common@v0.0.0-20210418235824-61aa2fff3a37
# .... fc-retrieval-common@v0.0.0-20210504153526-e02678d1b531


override module dependency on local source
==
# inside go.mod, edit:
require (
	github.com/ConsenSys/fc-retrieval-common v0.0.0-20210418235824-61aa2fff3a37
    ....
)
replace github.com/ConsenSys/fc-retrieval-common => ../fc-retrieval-common


display modules/dependencies for the path 
==
go list -m github.com/ConsenSys/fc-retrieval-common


show available module updates
==
go list -m -u all
